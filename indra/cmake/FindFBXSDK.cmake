# -*- cmake -*-

# - Find natively installed FBX SDK headers and lib
# This module defines
#  FBXSDK_INCLUDE_DIR, where to find fmod.h and fmod_errors.h
#  FBXSDK_LIBRARIES, the libraries needed to use FBXSDK.
#  FBXSDK, If false, do not try to use FBXSDK.
# also defined, but not for general use are
#  FBXSDK_LIBRARY, where to find the FBXSDK library.

FIND_PATH(FBXSDK_INCLUDE_DIR fbxsdk.h PATH_SUFFIXES fbxsdk)

SET(FBXSDK_NAMES ${FBXSDK_NAMES} fbxsdk fbxsdk-md fbxsdk-mt)
FIND_LIBRARY(FBXSDK_LIBRARY
  NAMES ${FBXSDK_NAMES}
  PATH_SUFFIXES fbxsdk
  )

IF (FBXSDK_FOUND)
  IF (NOT FBXSDK_FIND_QUIETLY)
    MESSAGE(STATUS "Found FBXSDK: ${FBXSDK_LIBRARIES}")
  ENDIF (NOT FBXSDK_FIND_QUIETLY)
ELSE (FBXSDK_FOUND)
  IF (FBXSDK_FIND_REQUIRED)
    MESSAGE(FATAL_ERROR "Could not find FBXSDK library")
  ENDIF (FBXSDK_FIND_REQUIRED)
ENDIF (FBXSDK_FOUND)

# Deprecated declarations.
SET (NATIVE_FBXSDK_INCLUDE_PATH ${FBXSDK_INCLUDE_DIR} )
GET_FILENAME_COMPONENT (NATIVE_FBXSDK_LIB_PATH ${FBXSDK_LIBRARY} PATH)

MARK_AS_ADVANCED(
  FBXSDK_LIBRARY
  FBXSDK_INCLUDE_DIR
  )
