# -*- cmake -*-
include(Prebuilt)
use_prebuilt_binary(fbxsdk)    
set(FBXSDK_LIBRARY libfbxsdk-md)
set(FBXSDK_LIBRARIES ${FBXSDK_LIBRARY})
set(FBXSDK_INCLUDE_DIR ${LIBS_PREBUILT_DIR}/include/fbxsdk)

