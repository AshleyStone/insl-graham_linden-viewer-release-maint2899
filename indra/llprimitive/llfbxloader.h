/**
 * @file llfbxloader.h
 * @brief LLDAELoader class definition
 *
 * $LicenseInfo:firstyear=2013&license=viewerlgpl$
 * Second Life Viewer Source Code
 * Copyright (C) 2013, Linden Research, Inc.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Linden Research, Inc., 945 Battery Street, San Francisco, CA  94111  USA
 * $/LicenseInfo$
 */

#ifndef LL_LLFBXLOADER_H
#define LL_LLFBXLOADER_H

//#define USE_FBX_SDK 1

#if USE_FBX_SDK

// enables new API in 2013.3 or better...
//
#define FBXSDK_NEW_API   1

#if LL_MSVC
// for the love of all that is holy, STFU!
#pragma warning (disable : 4263)
#pragma warning (disable : 4264)
#endif

#include <fbxsdk.h>
#include "llmodelloader.h"
#include "v4color.h"

class LLFBXLoader : public LLModelLoader
{
public:	
	typedef std::map<FbxNode*, std::vector<LLPointer<LLModel> > >	fbx_model_map;

	LLFBXLoader(
		std::string									filename,
		S32											lod, 
		LLModelLoader::load_callback_t		load_cb,
		LLModelLoader::joint_lookup_func_t	joint_lookup_func,
		LLModelLoader::texture_load_func_t	texture_load_func,
		LLModelLoader::state_callback_t		state_cb,
		void*											opaque_userdata,
		JointTransformMap&						joint_map,
		JointSet&									joints_from_nodes);

	virtual ~LLFBXLoader();

	// Distinct init to allow FBX creation ops to fail AND be handled
	//
	virtual bool Init();
	virtual bool OpenFile(const std::string& filename);

protected:

	bool load();

	bool loadNode(FbxNode* node);
	bool loadMesh(FbxNode* node);
	bool loadTransform(FbxNode* node, LLMatrix4& transform);
	bool loadLight(FbxNode* node);
	bool loadSkeleton(FbxNode* node);
	bool loadLODGroup(FbxNode* node);	
	bool loadLODMesh(FbxLODGroup* lod_group, FbxNode* node);

	bool loadMaterials(LLModel* model, FbxGeometry* instance_geo, material_map& map);
	bool loadTexture(LLImportMaterial& material, FbxTexture* pTexture, int blend_mode = 0);

	static LLColor4 convertColor(const FbxColor& fbx_color)
	{
		return LLColor4(fbx_color[0],fbx_color[1],fbx_color[2],1.0);
	}

	static LLVector4a convertVector(const FbxVector4& in)
	{
		LLVector4a out;
		F32* pElement = out.getF32ptr();
		pElement[0] = in[0];
		pElement[1] = in[1];
		pElement[2] = in[2];
		pElement[3] = in[3];
		return out;
	}

	static LLVector4a convertNormal(const FbxVector4& in)
	{
		LLVector4a out;
		F32* pElement = out.getF32ptr();
		pElement[0] = in[0];
		pElement[1] = in[1];
		pElement[2] = in[2];
		pElement[3] = in[3];
		out.normalize3fast();
		return out;
	}

	static LLVector4a convertUV(const FbxVector2& in)
	{
		LLVector4a out;
		F32* pElement = out.getF32ptr();
		pElement[0] = in[0];
		pElement[1] = in[1];
		pElement[2] = 0.0f;
		pElement[3] = 1.0f;
		return out;
	}

	template<
		typename LindenType,
		typename FbxElemType,
		typename FbxDataType,
		LindenType (*ConvertFunc)(const FbxDataType& data) >
	static LindenType find(FbxElemType* elem, int control_point_index, int vertex_index)
	{
		int id = (elem->GetMappingMode() == FbxGeometryElement::eByControlPoint) ? control_point_index : vertex_index;

		//deref using a direct offset or an offset into the index table as appropriate
		//
		switch (elem->GetReferenceMode())
		{
		case FbxGeometryElement::eDirect:
			return (*ConvertFunc)(elem->GetDirectArray().GetAt(id));

		case FbxGeometryElement::eIndexToDirect:
			return (*ConvertFunc)(elem->GetDirectArray().GetAt(elem->GetIndexArray().GetAt(id)));

		default:
			break;
		}		
		return LindenType();
	}

	template< typename FbxElemType >
	LLColor4 findColor(FbxElemType* elem,int control_point_index, int vertex_index) const
	{
		return find<LLColor4, FbxElemType, FbxColor, convertColor >(elem, control_point_index, vertex_index);
	}

	template< typename FbxElemType >
	LLVector4a findUV(FbxElemType* elem,int control_point_index, int vertex_index) const
	{
		return find<LLVector4a, FbxElemType, FbxVector2, convertUV >(elem, control_point_index, vertex_index);
	}

	template< typename FbxElemType >
	LLVector4a findNormal(FbxElemType* elem,int control_point_index, int vertex_index) const
	{
		return find<LLVector4a, FbxElemType, FbxVector4, convertNormal >(elem, control_point_index, vertex_index);
	}

	template< typename FbxElemType >
	LLVector4a findTangent(FbxElemType* elem,int control_point_index, int vertex_index) const
	{
		return find<LLVector4a, FbxElemType, FbxVector4, convertNormal >(elem, control_point_index, vertex_index);
	}	

	template< typename FbxElemType >
	LLVector4a findBinorm(FbxElemType* elem,int control_point_index, int vertex_index) const
	{
		return find<LLVector4a, FbxElemType, FbxVector4, convertNormal >(elem, control_point_index, vertex_index);
	}

	template< typename FbxElemType >
	LLVector4a findVector(FbxElemType* elem,int control_point_index, int vertex_index) const
	{
		return find<LLVector4a, FbxElemType, FbxVector4, convertVector >(elem, control_point_index, vertex_index);
	}

	int sdk_major;
	int sdk_minor;
	int sdk_rev;
	int file_major;
	int file_minor;
	int file_rev;
	int file_format;
	int anim_stack_count;

	FbxManager*		mFbxManager;
	FbxScene*		mFbxScene;
	bool				mResult;
	fbx_model_map	mModelsMap;
};

#endif  // USE_FBX_SDK
#endif  // LL_LLFBXLOADER_H
