/**
 * @file lldaeloader.cpp
 * @brief LLDAELoader class implementation
 *
 * $LicenseInfo:firstyear=2013&license=viewerlgpl$
 * Second Life Viewer Source Code
 * Copyright (C) 2013, Linden Research, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * Linden Research, Inc., 945 Battery Street, San Francisco, CA  94111  USA
 * $/LicenseInfo$
 */

#include "llfbxloader.h"

#if USE_FBX_SDK

#include "llsdserialize.h"
#include "lljoint.h"

#include "glh/glh_linear.h"
#include "llmatrix4a.h"
#include "m4math.h"

#if LL_MSVC
// many unused params during dev, ignore for now
#pragma warning (disable : 4189)
#endif

//-----------------------------------------------------------------------------
// LLFBXLoader
//-----------------------------------------------------------------------------
LLFBXLoader::LLFBXLoader(
	std::string				filename,
	S32						lod,
	load_callback_t		load_cb,
	joint_lookup_func_t	joint_lookup_func,
	texture_load_func_t	texture_load_func,
	state_callback_t		state_cb,
	void*						opaque_userdata,
	JointTransformMap&	jointMap,
	JointSet&				jointsFromNodes )
: LLModelLoader(
		filename,
		lod,
		load_cb,
		joint_lookup_func,
		texture_load_func,
		state_cb,
		opaque_userdata,
		jointMap,
		jointsFromNodes)
, mFbxManager(0)
, mFbxScene(0)
, mResult(false)
{
}

LLFBXLoader::~LLFBXLoader()
{
	if(mFbxManager)
	{
		mFbxManager->Destroy();
	}
	mFbxManager = NULL;

}

bool LLFBXLoader::OpenFile(const std::string& filename)
{	
	// Prepare the FBX SDK.
	if (!Init())
	{
		setLoadState( ERROR_PARSING );
		return false;
	}

	mFbxManager->GetFileFormatVersion(sdk_major, sdk_minor, sdk_rev);

	FbxImporter* importer = FbxImporter::Create(mFbxManager,"FBXLoader");
	llassert(importer);

	const bool status = importer->Initialize(filename.c_str(), -1, mFbxManager->GetIOSettings());

	importer->GetFileVersion(file_major, file_minor, file_rev);

	if( !status )
	{
		FbxString error = importer->GetStatus().GetErrorString();
		setLoadState( ERROR_LOADER_SETUP );
		importer->Destroy();
		return false;
	}

	if (!importer->IsFBX())
	{
		setLoadState( ERROR_INVALID_FILE );
		mResult = false;
	}
	else
	{
		// Import the scene.
		mResult = importer->Import(mFbxScene);

		FbxStatus::EStatusCode status_code = importer->GetStatus().GetCode();

		// Cover some not-so-bright cases where FBX reports true despite failing
		//
		switch(status_code)
		{
			case FbxStatus::eSuccess:
			default:
				break; // rock on

			case FbxStatus::eFailure:					setLoadState( ERROR_PARSING );					mResult = false; break;
			case FbxStatus::eInvalidFile:				setLoadState( ERROR_INVALID_FILE );				mResult = false; break;
			case FbxStatus::ePasswordError:			setLoadState( ERROR_PASSWORD_REQUIRED );		mResult = false; break;
			case FbxStatus::eInsufficientMemory:	setLoadState( ERROR_NEED_MORE_MEMORY );		mResult = false; break;
			case FbxStatus::eInvalidParameter:		setLoadState( ERROR_INVALID_PARAMETERS );		mResult = false; break;
			case FbxStatus::eIndexOutOfRange:		setLoadState( ERROR_OUT_OF_RANGE );				mResult = false; break;
			case FbxStatus::eInvalidFileVersion:	setLoadState( ERROR_FILE_VERSION_INVALID );	break; // CAN still work correctly, so don't report as a failure
		}
	}

	if(mResult == false)
	{
		FbxString error = importer->GetStatus().GetErrorString();
		llwarns << "FBX Parsing of " << filename << " failed with error " << error << llendl;
	}

	// Destroy the importer.
	importer->Destroy();

	if(mResult == false)
	{
		return false;
	}
	else 
	{
		// Walk the parsed data,
		// creating our own structures
		// for model storage
		//
		mResult = load();
	}

	setLoadState( DONE );

	return true;
}

bool LLFBXLoader::load()
{
	if (!mFbxScene)
		return false;

	FbxNode* root_node = mFbxScene->GetRootNode();
	return root_node ? loadNode(root_node) : false;
}

bool LLFBXLoader::Init()
{
	//The first thing to do is to create the FBX Manager which is the object allocator for almost all the classes in the SDK
	mFbxManager = FbxManager::Create();
	if( !mFbxManager )
	{
		llerrs << "Failed to create FBX SDK Manager" << llendl;
		return false;
	}

	FbxIOSettings* ios = FbxIOSettings::Create(mFbxManager, IOSROOT);

	// Disable parsing parts we don't want or can't support
	//
	//ios->SetBoolProp(EXP_FBX_GLOBAL_SETTINGS,	false);
	//ios->SetBoolProp(EXP_FBX_EMBEDDED,			false);
	//ios->SetBoolProp(EXP_FBX_SHAPE,				false);
	//ios->SetBoolProp(EXP_FBX_TEMPLATE,			false);
	//ios->SetBoolProp(EXP_FBX_PIVOT,				false);
	//ios->SetBoolProp(EXP_FBX_GLOBAL_SETTINGS,	false);
	//ios->SetBoolProp(EXP_FBX_CHARACTER,			false);
	//ios->SetBoolProp(EXP_FBX_CONSTRAINT,		false);
	//ios->SetBoolProp(EXP_FBX_ANIMATION,			false);

	ios->SetBoolProp(IMP_FBX_TEMPLATE, false);
	ios->SetBoolProp(IMP_FBX_PIVOT, false);
	ios->SetBoolProp(IMP_FBX_GLOBAL_SETTINGS, false);
	ios->SetBoolProp(IMP_FBX_CHARACTER, false);
	ios->SetBoolProp(IMP_FBX_CONSTRAINT, false);
	ios->SetBoolProp(IMP_FBX_MERGE_LAYER_AND_TIMEWARP, false);
	ios->SetBoolProp(IMP_FBX_GOBO, false);
	ios->SetBoolProp(IMP_FBX_SHAPE, false);
	ios->SetBoolProp(IMP_FBX_LINK, false);
	ios->SetBoolProp(IMP_FBX_ANIMATION, false);
	ios->SetBoolProp(IMP_FBX_CURRENT_TAKE_NAME, false);
	ios->SetBoolProp(IMP_FBX_EXTRACT_EMBEDDED_DATA, false);

	mFbxManager->SetIOSettings(ios);

#if USE_FBX_PLUGINS
	mFbxManager->LoadPluginsDirectory(FbxGetApplicationDirectory().Buffer());
#endif

	//Create an FBX scene. This object holds most objects imported/exported from/to files.
	mFbxScene = FbxScene::Create(mFbxManager, "FBXLoaderScene");
	llassert(mFbxScene);

	return true;
}

bool LLFBXLoader::loadMaterials(LLModel* model, FbxGeometry* instance_geo, LLFBXLoader::material_map& map)
{
	llassert(instance_geo);	

	int material_count = 0;

	FbxNode* geo_node = NULL;
	if(instance_geo)
	{
		geo_node = instance_geo->GetNode();
		if(!geo_node)
		{
			return false;
		}
		material_count = geo_node->GetMaterialCount();    
	}

	int material_index;
	for (material_index = 0; material_index < material_count; material_index++)
	{
		FbxSurfaceMaterial*	fbx_material = instance_geo->GetNode()->GetSrcObject< FbxSurfaceMaterial >(material_index);
		if(fbx_material)
		{
			if (fbx_material->GetName())
			{
				model->mMaterialList.push_back(fbx_material->GetName());
			}			

			LLImportMaterial import_material;

			// See if we have an GLSL hardware shader
			//
			const FbxImplementation* shader_impl = GetImplementation(fbx_material, FBXSDK_IMPLEMENTATION_OGS);

			if(shader_impl)
			{
				//Now we have a GSL hardware shader, let's read it
				//
				const FbxBindingTable* root_table = shader_impl->GetRootTable();

				llassert(root_table);
				if (!root_table)
				{
					continue;
				}

				FbxString shader_file_name	= root_table->DescAbsoluteURL.Get();
				FbxString shader_technique	= root_table->DescTAG.Get(); 

				int entries = (int)root_table->GetEntryCount();
				for(int i=0; i < entries; ++i)
				{
					const FbxBindingTableEntry& entry = root_table->GetEntry(i);
					const char* entry_type = entry.GetEntryType(true); 

					// Find the property in the binding table
					// or constants as appropriate
					//
					FbxProperty lFbxProp;
					if (!strcmp(FbxPropertyEntryView::sEntryType, entry_type))
					{   
						lFbxProp = fbx_material->FindPropertyHierarchical(entry.GetSource()); 
						if(!lFbxProp.IsValid())
						{
							lFbxProp = fbx_material->RootProperty.FindHierarchical(entry.GetSource());
						}
					}
					else if(!strcmp(FbxConstantEntryView::sEntryType, entry_type))
					{
						lFbxProp = shader_impl->GetConstants().FindHierarchical(entry.GetSource());
					}

					if(lFbxProp.IsValid())
					{
						if( lFbxProp.GetSrcObjectCount<FbxTexture>() > 0 )
						{
							for(int j=0; j<lFbxProp.GetSrcObjectCount<FbxFileTexture>(); ++j)
							{
								FbxTexture* texture = lFbxProp.GetSrcObject<FbxFileTexture>(j);
								if(texture)
								{
									loadTexture(import_material, texture);
								}
							}
						}
						/*
						   your shader parms here...
						else
						{
							FbxDataType lFbxType = lFbxProp.GetPropertyDataType();
							FbxString blah			= lFbxType.GetName();

							if(FbxBoolDT == lFbxType)
							{
								lFbxProp.Get<FbxBool>();
							}
							else if ( FbxIntDT == lFbxType ||  FbxEnumDT  == lFbxType )
							{
								lFbxProp.Get<FbxInt>();
							}
							else if ( FbxFloatDT == lFbxType)
							{
								lFbxProp.Get<FbxFloat>();

							}
							else if ( FbxDoubleDT == lFbxType)
							{
								lFbxProp.Get<FbxDouble>();
							}
							else if ( FbxStringDT == lFbxType
								||  FbxUrlDT  == lFbxType
								||  FbxXRefUrlDT  == lFbxType )
							{
							}
							else if ( FbxDouble2DT == lFbxType)
							{
								lFbxProp.Get<FbxDouble2>();
							}
							else if ( FbxDouble3DT == lFbxType || FbxColor3DT == lFbxType)
							{
								lFbxProp.Get<FbxDouble3>();
							}
							else if ( FbxDouble4DT == lFbxType || FbxColor4DT == lFbxType)
							{
								lFbxProp.Get<FbxDouble4>();
							}
							else if ( FbxDouble4x4DT == lFbxType)
							{
								lFbxProp.Get<FbxDouble4x4>();
							}
						}*/
					}   
				}
			} // if(shader_impl)

			// Phong material, interpret params!
			//
			if (fbx_material->GetClassId().Is(FbxSurfacePhong::ClassId))
			{
				FbxSurfacePhong* phong = static_cast< FbxSurfacePhong* >(fbx_material);

				import_material.mDiffuseColor			= convertColor(phong->Diffuse.Get());
				import_material.mDiffuseColor.mV[3] = phong->TransparencyFactor.Get();
				//import_material.mSpecularColor		= convertColor(phong->Specular.Get());
				//import_material.mShiny				= phong->Shininess.Get();
				//import_material.mEnvIntensity		= phong->ReflectionFactor.Get();
				//import_material.mDiffuseAlphaMode = LLMaterial::DIFFUSE_ALPHA_MODE_NONE;
				//import_material.mFullbright			= false;
			}

			if (fbx_material->GetClassId().Is(FbxSurfaceLambert::ClassId))
			{
				FbxSurfaceLambert* lambert = static_cast< FbxSurfaceLambert* >(fbx_material);
				import_material.mDiffuseColor			= convertColor(lambert->Diffuse.Get());
				import_material.mDiffuseColor.mV[3] = lambert->TransparencyFactor.Get();
				//import_material.mSpecularColor		= LLColor4::black;
				//import_material.mShiny				= 0.0f;
				//import_material.mEnvIntensity		= 0.0f;
				//import_material.mDiffuseAlphaMode = LLMaterial::DIFFUSE_ALPHA_MODE_NONE;
				//import_material.mFullbright			= false;
			}

			int texture_index;
			FBXSDK_FOR_EACH_TEXTURE(texture_index)
			{
				FbxProperty prop = fbx_material->FindProperty(FbxLayerElement::sTextureChannelNames[texture_index]);

				if(prop.IsValid())
				{
					int texture_count = prop.GetSrcObjectCount<FbxTexture>();
					for (int j = 0; j < texture_count; ++j)
					{
						FbxLayeredTexture *layered_texture = prop.GetSrcObject<FbxLayeredTexture>(j);
						if (layered_texture)
						{
							int layered_tex_count = layered_texture->GetSrcObjectCount<FbxTexture>();
							for(int k =0; k< layered_tex_count; ++k)
							{
								FbxTexture* texture = layered_texture->GetSrcObject<FbxTexture>(k);
								if(texture)
								{
									// Must use 'master' blend mode on layered tex instance to prevent
									// confusion when a given tex is used repeatedly with diff modes
									//
									FbxLayeredTexture::EBlendMode layered_blend_mode;
									layered_texture->GetTextureBlendMode(k, layered_blend_mode);
									loadTexture(import_material,texture,layered_blend_mode);
								}
							}
						}
						else
						{
							FbxTexture* texture = prop.GetSrcObject<FbxTexture>(j);
							if(texture)
							{
								loadTexture(import_material, texture);
							}
						}
					}
				}
			}

			if (model->mMaterialList.size() > material_index)
			{
				import_material.mBinding = model->mMaterialList[material_index];
				map[model->mMaterialList[material_index]] = import_material;
			}
		}
	}

	return true;
}

#define IMPORT(a)			case FbxNodeAttribute::e##a: result = LLFBXLoader::load##a(pNode); break;
#define IGNORE_NODE(a)	case FbxNodeAttribute::e##a:

bool LLFBXLoader::loadNode(FbxNode* pNode)
{
	bool result = true;
	if(pNode->GetNodeAttribute())
	{
		FbxNodeAttribute::EType node_attr_type = pNode->GetNodeAttribute()->GetAttributeType();
		switch (node_attr_type)
		{
			// Stuff we don't care about loading
			//
			IGNORE_NODE(Marker)
			IGNORE_NODE(Nurbs)
			IGNORE_NODE(Patch)
			IGNORE_NODE(Camera)
			default:
				return true;

			// Stuff we do
			//
			IMPORT(Mesh)
			IMPORT(Light)
			IMPORT(Skeleton)
			IMPORT(LODGroup)
		}
	}

	if (result)
	{
		LLMatrix4 transform;
		result = loadTransform(pNode, transform);
	}

#if USE_FBX_TARGETS
	if (pNode->GetTarget())
	{
			loadTarget(pNode);
	}
#endif

	for(int i = 0; result && i < pNode->GetChildCount(); i++)
	{
		result = loadNode(pNode->GetChild(i));
	}

	return result;
}

bool LLFBXLoader::loadMesh(FbxNode* pNode)
{
	LL_ALIGN_16(std::vector<LLVolumeFace::VertexData>	verts);
	LL_ALIGN_16(std::vector<U16>								indices);

	LLVolumeFace face;

	FbxMesh* mesh = static_cast< FbxMesh* >(pNode->GetNodeAttribute());
	if (!mesh)
	{
		return false;
	}

	LLVolumeParams volume_params;	
	volume_params.setType(LL_PCODE_PROFILE_SQUARE, LL_PCODE_PATH_LINE);

	LLModel* model = new LLModel(volume_params, 0.f); 

	// The astute may wonder why this is necessary with a freshly created model,
	// but it is required for correct indexing of materials and volume faces.
	//
	model->ClearFacesAndMaterials();

	model->mLabel = (char*)pNode->GetName();


	int i;
	int j;
	int			control_point_count	= mesh->GetControlPointsCount();
	int			normal_count			= mesh->GetElementNormalCount();
	int			polygon_count			= mesh->GetPolygonCount();	
	int			uv_count					= mesh->GetElementUVCount();
	int			vertex_color_count	= mesh->GetElementVertexColorCount();
	int			tangent_count			= mesh->GetElementTangentCount();
	int			binorm_count			= mesh->GetElementBinormalCount();
	int			poly_group_count		= mesh->GetElementPolygonGroupCount();	
	int			material_count			= mesh->GetElementMaterialCount();

	FbxVector4* control_points			= mesh->GetControlPoints();


	if (control_point_count)
	{
		face.mExtents[0].set(control_points[0][0], control_points[0][1], control_points[0][2]);
		face.mExtents[1].set(control_points[0][0], control_points[0][1], control_points[0][2]);
	}

	for(i = 0; i < control_point_count; i++)
	{
		LLVolumeFace::VertexData cv;
		cv.setPosition(convertVector(control_points[i]));
		verts.push_back(cv);

		// Update min/max extents
		//
		update_min_max(face.mExtents[0], face.mExtents[1], cv.getPosition());
	}

	int vertex_index = 0;
	for (i = 0; i < polygon_count; i++)
	{
		int l;
		
#if USE_FBX_POLY_GROUPS
		for (l = 0; l < poly_group_count; l++)
		{
			FbxGeometryElementPolygonGroup* poly_group = mesh->GetElementPolygonGroup(l);
			
			llassert((poly_group->GetMappingMode() == FbxGeometryElement::eByPolygon)
				   && (poly_group->GetReferenceMode() == FbxGeometryElement::eIndex));

			int poly_group_index = poly_group->GetIndexArray().GetAt(i);
		}
#endif

		int polygon_size = mesh->GetPolygonSize(i);

		for (j = 0; j < polygon_size; j++)
		{
			int control_point_index = mesh->GetPolygonVertex(i, j);

			for (l = 0; l < uv_count; ++l)
			{
				LLVector4a tc = findUV(mesh->GetElementUV(l), control_point_index, vertex_index);
				verts[control_point_index].mTexCoord.set(tc.getF32ptr());
			}

			for( l = 0; l < normal_count; ++l)
			{
				verts[control_point_index].setNormal(findNormal(mesh->GetElementNormal(l), control_point_index, vertex_index));
			}

#if USE_FBX_TANGENT_BASIS_AND_VERTEX_COLORS
			for( l = 0; l < tangent_count; ++l)
			{
				verts_ex[l].tangent = findTangent(mesh->GetElementTangent(l),control_point_index,vertex_index);
			}

			for( l = 0; l < tangent_count; ++l)
			{
				verts_ex[l].tangent = findTangent(mesh->GetElementTangent(l),control_point_index,vertex_index);
			}

			for (l = 0; l < vertex_color_count; l++)
			{
				verts_ex[l].vertex_color = findColor(mesh->GetElementVertexColor(l), control_point_index, vertex_index);
			}
#endif
			vertex_index++;

		} // for polygonSize

		// triangle!
		//
		if (polygon_size == 3)
		{
			int control_point_index_a = mesh->GetPolygonVertex(i, 0);
			int control_point_index_b = mesh->GetPolygonVertex(i, 1);
			int control_point_index_c = mesh->GetPolygonVertex(i, 2);
			indices.push_back(control_point_index_a);
			indices.push_back(control_point_index_b);
			indices.push_back(control_point_index_c);
		}
		// quad busted into,...2 triangles!
		//
		else if(polygon_size == 4)
		{
			int control_point_index_a = mesh->GetPolygonVertex(i, 0);
			int control_point_index_b = mesh->GetPolygonVertex(i, 1);
			int control_point_index_c = mesh->GetPolygonVertex(i, 3);
			int control_point_index_d = mesh->GetPolygonVertex(i, 2);

			indices.push_back(control_point_index_a);
			indices.push_back(control_point_index_b);
			indices.push_back(control_point_index_c);
			indices.push_back(control_point_index_d);
			indices.push_back(control_point_index_c);
			indices.push_back(control_point_index_b);
		}			
		// N-gon busted into, you guessed it! N-2 triangles!
		// (very not sure about winding order issues with this)
		// would be much better to have the source assets triangulated
		// and simply reject this asset, but this may do.
		//
		else
		{
			int control_point_index_a = mesh->GetPolygonVertex(i, 0);
			U32 s;
			for (s = 0; s < (polygon_size - 2); s++)
			{
				indices.push_back(control_point_index_a);
				indices.push_back(mesh->GetPolygonVertex(i, s + 1));
				indices.push_back(mesh->GetPolygonVertex(i, s + 2));
			}
		}

	} // for polygonCount
	
	for (int m = 0; m < material_count; m++)
	{
		FbxGeometryElementMaterial* material_elem = mesh->GetElementMaterial(m);
		if (material_elem)
		{
			int index_count = material_elem->GetIndexArray().GetCount(); 

			// all indices referencing this material...
			//
			for (int idx_idx = 0; idx_idx < index_count; idx_idx++)
			{
				material_elem->GetIndexArray().GetAt(idx_idx);
			}
		}
	}

	if (!verts.size())
	{
		return false;
	}

	if (!indices.size())
	{
		return false;
	}
	
	face.fillFromLegacyData(verts, indices);

	LLModelLoader::material_map materials;
	loadMaterials(model, mesh, materials);

	model->getVolumeFaces().push_back(face);

	static bool s_normalize_em = true;
	static bool s_optimize_em  = true;

	if (s_normalize_em)
	{
		model->normalizeVolumeFaces();
	}

	if (s_optimize_em)
	{
		model->optimizeVolumeFaces();
	}

	mModelList.push_back(model);
	mModelsMap[pNode].push_back(model);

	LLVector3 mesh_scale_vector;
	LLVector3 mesh_translation_vector;
	model->getNormalizedScaleTranslation(mesh_scale_vector, mesh_translation_vector);

	LLMatrix4 transformation;
	
	transformation.initScale(mesh_scale_vector);
	transformation.setTranslation(mesh_translation_vector);
	transformation *= mTransform;

	mScene[transformation].push_back(LLModelInstance(model, model->mLabel, transformation, materials));
	stretch_extents(model, mTransform, mExtents[0], mExtents[1], mFirstTransform);

	return true;
}

bool LLFBXLoader::loadTransform(FbxNode* pNode, LLMatrix4& transform)
{
	FbxVector4 xlat	= pNode->GetGeometricTranslation(FbxNode::eSourcePivot);
	FbxVector4 rot		= pNode->GetGeometricRotation(FbxNode::eSourcePivot);
	FbxVector4 scale	= pNode->GetGeometricScaling(FbxNode::eSourcePivot);

	LLVector3		ll_xlat(xlat[0],xlat[1],xlat[2]);
	LLQuaternion	ll_q_rot(rot[0],rot[1],rot[2],rot[3]);
	LLVector3		ll_scale(scale[0],scale[1],scale[2]);

	transform.setIdentity();
	transform.initScale(ll_scale);
	transform.rotate(ll_q_rot);
	transform.setTranslation(ll_xlat);	

	return true;
}

bool LLFBXLoader::loadLight(FbxNode* pNode)
{
	FbxLight* light = static_cast< FbxLight* >(pNode->GetNodeAttribute());
	if (!light)
	{
		return false;
	}

	enum FBXLightType
	{
		Point,
		Directional,
		Spot
	};

	FBXLightType light_type = (FBXLightType)light->LightType.Get();
	char *		 light_name	= (char *) pNode->GetName();

	bool has_filename = !(light->FileName.Get().IsEmpty());
	if (has_filename)
	{
		const char* projector_filename = (const char*)light->FileName.Get().Buffer();
		bool light_is_groundproject	 = light->DrawGroundProjection.Get();
		bool light_is_volumetric		 = light->DrawVolumetricLight.Get();
		bool light_is_billboard_volume = light->DrawFrontFacingVolumetricLight.Get();
	}

	LLColor4		light_color			= convertColor(light->Color.Get());
	F32			light_intensity	= light->Intensity.Get();
	F32			light_cone_angle	= light->OuterAngle.Get();
	F32			light_fog_density	= light->Fog.Get();

	return true;
}

bool LLFBXLoader::loadSkeleton(FbxNode* pNode)
{
	FbxSkeleton* skelement = (FbxSkeleton*) pNode->GetNodeAttribute();

	if (!skelement)
	{
		return false;
	}

	FbxSkeleton::EType skel_type = skelement->GetSkeletonType();

	bool limb		= (skelement->GetSkeletonType() == FbxSkeleton::eLimb);
	bool limb_node = (skelement->GetSkeletonType() == FbxSkeleton::eLimb);
	bool skel_root	= (skelement->GetSkeletonType() == FbxSkeleton::eRoot);

	F32  limb_length		= limb ? skelement->LimbLength.Get()					: 0.0f;
	F32  limb_node_size	= (limb_node || skel_root) ? skelement->Size.Get() : 0.0f;

	return true;
}

bool LLFBXLoader::loadLODGroup(FbxNode* pNode)
{
	bool status = true;

	const char* lod_node_name = (const char *)pNode->GetName();
	FbxLODGroup* lod_group = (FbxLODGroup*)pNode->GetNodeAttribute();

	if (lod_group->MinMaxDistance.Get())
	{
		F32 min_dist = lod_group->MinDistance.Get();
		F32 max_dist = lod_group->MaxDistance.Get();
	}

	bool world_space_distances = lod_group->WorldSpace.Get();

	std::vector<F32> thresholds;
	for (int i = 0; i < lod_group->GetNumThresholds(); i++)
	{
		FbxDistance lThreshVal;
		if (lod_group->GetThreshold(i, lThreshVal))
		{
			thresholds.push_back(lThreshVal.value());
		}
	}

	for (int i = 0; status && (i < pNode->GetChildCount()); i++)
	{
		status = loadLODMesh(lod_group, pNode->GetChild(i));
	}

	return status;
}


bool LLFBXLoader::loadLODMesh(FbxLODGroup* lod_group,FbxNode* pNode)
{
	bool status = loadMesh(pNode);
	// other stuff?
	return status;
}

bool LLFBXLoader::loadTexture(LLImportMaterial& import_material, FbxTexture* texture, int blend_mode)
{
	FbxFileTexture*			file_texture = FbxCast<FbxFileTexture>(texture);
	FbxProceduralTexture*	proc_texture = FbxCast<FbxProceduralTexture>(texture);
	
	import_material.mDiffuseMapLabel = reinterpret_cast< const char * >(texture->GetName());

	if (file_texture)
	{
		import_material.mDiffuseMapFilename = reinterpret_cast< const char * >(file_texture->GetFileName());
	}
	else if (proc_texture)
	{
		return false;
	}

	F32										scale_u		= texture->GetScaleU();
	F32										scale_v		= texture->GetScaleV();
	F32										xlat_u		= texture->GetTranslationU();
	F32										xlat_v		= texture->GetTranslationV();
	F32										rot_u			= texture->GetRotationU();
	F32										rot_v			= texture->GetRotationV();
	FbxTexture::EAlphaSource			alpha_src	= texture->GetAlphaSource();
	FbxTexture::EMappingType			mapping		= texture->GetMappingType();
	FbxTexture::EPlanarMappingNormal	norm_dir		= texture->GetPlanarMappingNormal();
	FbxTexture::EBlendMode				blendmode	= (FbxTexture::EBlendMode)blend_mode;
	F32										alpha			= texture->GetDefaultAlpha();

	return true;
}

#endif // USE_FBX_SDK

